﻿using Microsoft.EntityFrameworkCore;
using ProductsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsAPI.DbContexts
{
    public class ProductAPIDbconstext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public ProductAPIDbconstext(DbContextOptions<ProductAPIDbconstext> options) : base(options)
        {

        }
    }
}
