﻿using Microsoft.EntityFrameworkCore;
using ProductsAPI.DbContexts;
using ProductsAPI.Models;
using ProductsAPI.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsAPI.Services
{
    public class ProductService : IProductService
    {
        private readonly ProductAPIDbconstext _dbcontext;

        public ProductService(ProductAPIDbconstext dbconstext)
        {
            _dbcontext = dbconstext;
        }
        public async Task<Product> AddProductAsync(Product product)
        {
            try
            {

                await _dbcontext.Products.AddAsync(product);
                await _dbcontext.SaveChangesAsync();
                return product;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task DeleteProductAsync(int productId)
        {
            try
            {
                
                var product = await GetProductByIdAsync(productId);
                if (product !=null)
                {
                    _dbcontext.Products.Remove(product);
                    await _dbcontext.SaveChangesAsync();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<List<Product>> GetProductByCategotyAsync(int categoryId)
        {
            try
            {

                var products = await _dbcontext.Products.Where(x => x.CategoryID == categoryId).ToListAsync();
                return products;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Product> GetProductByIdAsync(int productId)
        {
            try
            {

                var product = await _dbcontext.Products.Where( x => x.ID == productId).FirstOrDefaultAsync();
                return product;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<List<Product>> GetProductsAsync()
        {
            try
            {

                var products = await _dbcontext.Products.ToListAsync();
                return products;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Product> UpdateProductAsync(Product product)
        {
            try
            {
                if (product.ID > 0)
                {
                    _dbcontext.Entry(product).State = EntityState.Modified;
                    await _dbcontext.SaveChangesAsync();
                    return product;
                }

                throw new Exception("Product Not Found");
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
