﻿using Microsoft.EntityFrameworkCore;
using ProductsAPI.DbContexts;
using ProductsAPI.Models;
using ProductsAPI.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsAPI.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ProductAPIDbconstext _dbconstext;
        public CategoryService(ProductAPIDbconstext dbcontext)
        {
            _dbconstext = dbcontext;
        }

        public async Task<Category> AddCategoryAsync(Category category)
        {
            try
            {
                await _dbconstext.Categories.AddAsync(category);
                await _dbconstext.SaveChangesAsync();
                return category;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<Category>> GetCategoriesAsync()
        {
            try
            {

                var categories = await _dbconstext.Categories.ToListAsync();
                return categories;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Category> GetCategoryByIdAsync(int categoryId)
        {
            try
            {

                var category = await _dbconstext.Categories.Where(x=> x.ID == categoryId).FirstOrDefaultAsync();
                return category;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
