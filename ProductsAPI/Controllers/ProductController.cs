﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductsAPI.Models;
using ProductsAPI.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productServices;

        public ProductController(IProductService productServices)
        {
            _productServices = productServices;
        }

        [HttpGet]
        public async Task<ActionResult<List<Product>>> GetProductsAsync()
        {
            try
            {
                var products = await _productServices.GetProductsAsync();
                return Ok(products);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetByCategory/{categoryId:int}")]

        public async Task<ActionResult<List<Product>>> GetProductsByCategoryAsync(int categoryId)
        {
            try
            {
                var products = await _productServices.GetProductByCategotyAsync(categoryId);
                return Ok(products);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{productId:int}", Name = "GetProduct")]
        public async Task<ActionResult<Product>> GetProductsByIdAsync(int productId)
        {
            try
            {
                var product = await _productServices.GetProductByIdAsync(productId);
                if (product != null)
                {

                    return Ok(product);
                }
                return NotFound();
            }
            catch (Exception ex) { return BadRequest(ex.Message); }

        }

        [HttpPost]
        public async Task<ActionResult<Product>> AddProductAsync(Product product)
        {
            try
            {
                var savedProduct = await _productServices.AddProductAsync(product);
                return CreatedAtAction("GetProduct", new { productId = savedProduct.ID }, savedProduct);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("{productId:int}", Name = "Delete")]
        public async Task<ActionResult> DeleteProductAsync(int productId)
        {
            try
            {
                await _productServices.DeleteProductAsync(productId);
                return Ok("Deleted Succesfully");
            }
            catch (Exception ex) { return BadRequest(ex.Message); }

        }

        [HttpPut]

        public async Task<ActionResult<Product>> UpdateProductAsync(Product product)
        {
            try
            {
                var newProduct = await _productServices.UpdateProductAsync(product);
                return Ok(newProduct);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

    }
}
